<?php

namespace Tests\Feature\controller;

use App\Services\Agent\AgentServiceInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery\MockInterface;
use Tests\TestCase;

class AgentControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAssignRequest()
    {
        $res = [
            "result"    =>  true,
            "message"   =>  "assign",
            "data"      =>  null
        ];
        $this->mock(AgentServiceInterface::class, function (MockInterface $mock) {
            $mock->shouldReceive('assignToOrder')
                ->once()
                ->andReturn(30);
        });
        $response = $this->post('api/v1/agents/1/assign-request');
        $this->assertEquals($res, [
            "result"    =>  true,
            "message"   =>  "assign",
            "data"      =>  null
        ]);
        $response->assertStatus(200);
    }
}
