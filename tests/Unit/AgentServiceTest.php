<?php

namespace Tests\Unit;

use App\Models\Agent;
use App\Models\DelayQueue;
use App\Repositories\Agent\AgentRepositoryInterface;
use App\Repositories\DelayQueue\DelayQueueRepositoryInterface;
use App\Services\Agent\AgentServiceInterface;
use Mockery\MockInterface;
use Tests\TestCase;

class AgentServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testAssignToOrderWhenAllDelayQueuePending()
    {
        $this->mock(AgentRepositoryInterface::class, function (MockInterface $mock) {
            $mock->shouldReceive('allDelayQueuePendingCount')
                ->once()
                ->andReturn(1);
        });

        $res = app()->make(AgentServiceInterface::class)->assignToOrder(10);

        $this->assertEquals($res, [
            "result"    =>  false,
            "message"   =>  "not assign",
            "data"      =>  null
        ]);
    }
    public function testAssignToOrderWhenNull()
    {
        $this->mock(AgentRepositoryInterface::class, function (MockInterface $mock) {
            $mock->shouldReceive('allDelayQueuePendingCount')
                ->once()
                ->andReturn(0);
        });
        $this->mock(DelayQueueRepositoryInterface::class, function (MockInterface $mock) {
            $mock->shouldReceive('getDelayOrderFromQueue')
                ->once()
                ->andReturn(null);
        });

        $res = app()->make(AgentServiceInterface::class)->assignToOrder(10);

        $this->assertEquals($res, [
            "result"    =>  false,
            "message"   =>  "not assign",
            "data"      =>  null
        ]);
    }
    public function testAssignToOrder()
    {
        $this->mock(AgentRepositoryInterface::class, function (MockInterface $mock) {
            $mock->shouldReceive('allDelayQueuePendingCount')
                ->once()
                ->andReturn(0);
        });
        $this->mock(DelayQueueRepositoryInterface::class, function (MockInterface $mock) {
            $mock->shouldReceive('getDelayOrderFromQueue')
                ->once()
                ->andReturn(new DelayQueue());
            $mock->shouldReceive('updateDelayQueue')
                ->once()
                ->andReturn(new DelayQueue());
        });

        $res = app()->make(AgentServiceInterface::class)->assignToOrder(10);

        $this->assertEquals($res, [
        "result"    =>  true,
        "message"   =>  "assign",
        "data"      =>  null
        ]);
    }
}
