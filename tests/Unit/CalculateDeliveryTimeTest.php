<?php

namespace Tests\Unit;

use App\Services\CalculateDeliveryTime\CalculateDeliveryTimeInterface;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class CalculateDeliveryTimeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetDeliveryTime()
    {
        Http::fake([
            "http://run.mocky.io/v3/*" => Http::response([
                'status' => true,
                'data' => [
                    "eta"=>14
                ]
            ], 200)
        ]);

        $res = app()->make(CalculateDeliveryTimeInterface::class)->getDeliveryTime();
        self::assertEquals(14,$res);
    }
}
