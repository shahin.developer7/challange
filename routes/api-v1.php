<?php

use App\Http\Controllers\Api\V1\AgentController;
use App\Http\Controllers\Api\V1\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get("/",function (){
    return response()->json("Snappfood Challenge");
});

Route::post("orders/{id}/delay-request", [OrderController::class, "delayRequest"]);

Route::post("agents/{id}/assign-request", [AgentController::class, "assignRequest"]);

Route::get("delay-report", [OrderController::class, "delayReport"]);
