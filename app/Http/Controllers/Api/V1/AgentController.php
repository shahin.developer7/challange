<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\DelayQueue;
use App\Services\Agent\AgentServiceInterface;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    public $agentService;

    public function __construct(AgentServiceInterface $agentService)
    {
        $this->agentService = $agentService;
    }

    public function assignRequest($id, Request $request)
    {
        $result = $this->agentService->assignToOrder($id);
        return response()->json($result);
    }
}
