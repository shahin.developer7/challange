<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\DelayReportTypeEnum;
use App\Http\Controllers\Controller;
use App\Models\DelayQueue;
use App\Models\DelayReport;
use App\Models\Order;
use App\Models\Trip;
use App\Services\CalculateDeliveryTime\CalculateDeliveryTimeInterface;
use App\Services\Order\OrderServiceInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    public function delayRequest($id, Request $request)
    {
        $result = $this->orderService->orderDelaySubmit($id);
        return response()->json($result);
    }

    public function delayReport()
    {
        $result = $this->orderService->vendorOrderDelayReport();
        return response()->json($result);
    }
}
