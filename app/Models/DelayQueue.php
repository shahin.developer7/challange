<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DelayQueue extends Model
{
    protected $table = "delay_queue";

    protected $fillable = [
        "id",
        "delay_report_id",
        "order_id",
        "agent_id",
        "done"
    ];

    public function delay_report(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(DelayReport::class);
    }
    public function agent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Agent::class);
    }
}
