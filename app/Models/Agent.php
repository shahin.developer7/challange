<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $fillable = [
        "id",
        "name"
    ];

    public function allDelayQueue()
    {
        return $this->hasMany(DelayQueue::class);
    }
    public function allDelayQueueDone()
    {
        return $this->hasMany(DelayQueue::class)->where("done",1);
    }
    public function allDelayQueuePending()
    {
        return $this->hasMany(DelayQueue::class)->where("done",0);
    }

}
