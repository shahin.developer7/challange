<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        "id",
        "vendor_id",
        "time_delivery",
    ];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function trip()
    {
        return $this->hasOne(Trip::class);
    }

    public function delay_reports()
    {
        return $this->hasMany(DelayReport::class);
    }
}
