<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DelayReport extends Model
{
    protected $fillable = [
        "id",
        "order_id",
        "delay_time",
        "type",
    ];

    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

}
