<?php

namespace App\Models;

use App\Enums\TripStatusEnum;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
        "id",
        "order_id",
        "status"
    ];

    public const NewDelayStatues = [
        TripStatusEnum::ASSIGNED,
        TripStatusEnum::AT_VENDOR,
        TripStatusEnum::PICKED,
    ];

    public const DelayQueueStatues = [
        TripStatusEnum::DELIVERED
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
