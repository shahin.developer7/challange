<?php

namespace App\Providers;

use App\Repositories\Agent\AgentRepository;
use App\Repositories\Agent\AgentRepositoryInterface;
use App\Repositories\DelayQueue\DelayQueueRepository;
use App\Repositories\DelayQueue\DelayQueueRepositoryInterface;
use App\Repositories\DelayReport\DelayReportRepository;
use App\Repositories\DelayReport\DelayReportRepositoryInterface;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderRepositoryInterface;
use App\Repositories\Trip\TripRepository;
use App\Repositories\Trip\TripRepositoryInterface;
use App\Services\Agent\AgentService;
use App\Services\Agent\AgentServiceInterface;
use App\Services\CalculateDeliveryTime\CalculateDeliveryTimeInterface;
use App\Services\CalculateDeliveryTime\CalculateDeliveryTimeService;
use App\Services\Order\OrderService;
use App\Services\Order\OrderServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CalculateDeliveryTimeInterface::class, CalculateDeliveryTimeService::class);

        $this->app->singleton(AgentServiceInterface::class, AgentService::class);
        $this->app->singleton(OrderServiceInterface::class, OrderService::class);

        $this->app->singleton(AgentRepositoryInterface::class, AgentRepository::class);
        $this->app->singleton(DelayQueueRepositoryInterface::class, DelayQueueRepository::class);
        $this->app->singleton(DelayReportRepositoryInterface::class, DelayReportRepository::class);
        $this->app->singleton(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->singleton(TripRepositoryInterface::class, TripRepository::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
