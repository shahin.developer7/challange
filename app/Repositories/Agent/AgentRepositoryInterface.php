<?php

namespace App\Repositories\Agent;

interface AgentRepositoryInterface
{
    public function getAgentById($agent_id);
    public function allDelayQueuePendingCount($agent_id);
}
