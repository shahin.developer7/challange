<?php

namespace App\Repositories\Agent;

use App\Models\Agent;

class AgentRepository implements AgentRepositoryInterface
{

    public function getAgentById($agent_id)
    {
        return Agent::find($agent_id);
    }

    public function allDelayQueuePendingCount($agent_id)
    {
        $agent = Agent::find($agent_id);
        return $agent->allDelayQueuePending()->count();
    }
}
