<?php

namespace App\Repositories\Order;

use App\Models\Order;

interface OrderRepositoryInterface
{
    public function getOrderWithTripById($id);
    public function orderUpdate(Order $order, array $data);
}
