<?php

namespace App\Repositories\Order;

use App\Models\Order;

class OrderRepository implements OrderRepositoryInterface
{

    public function getOrderWithTripById($id)
    {
        return Order::with("trip")->where("id", $id)->first();
    }

    public function orderUpdate(Order $order, array $data)
    {
        return $order->fill($data)->save();
    }
}
