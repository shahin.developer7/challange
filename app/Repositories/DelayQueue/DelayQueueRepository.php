<?php

namespace App\Repositories\DelayQueue;

use App\Models\DelayQueue;

class DelayQueueRepository implements DelayQueueRepositoryInterface
{
    public function getDelayOrderFromQueue()
    {
        return DelayQueue::where("done", 0)->whereNull("agent_id")->orderBy("id", "ASC")->first();
    }

    public function updateDelayQueue($delayQueue,$data)
    {
        return $delayQueue->fill($data)->save();
    }

    public function getCountOfOrderPendingDelay($orderId)
    {
        return DelayQueue::where("order_id", $orderId)->where("done",0)->count();
    }

    public function addDelayQueue(array $data)
    {
        return DelayQueue::create($data);
    }
}
