<?php

namespace App\Repositories\DelayQueue;

use App\Models\DelayQueue;

interface DelayQueueRepositoryInterface
{
    public function getDelayOrderFromQueue();
    public function updateDelayQueue(DelayQueue $delayQueue, array $data);
    public function getCountOfOrderPendingDelay($orderId);
    public function addDelayQueue(array $data);
}
