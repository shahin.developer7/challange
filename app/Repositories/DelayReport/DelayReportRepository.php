<?php

namespace App\Repositories\DelayReport;

use App\Enums\DelayReportTypeEnum;
use App\Models\DelayReport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DelayReportRepository implements DelayReportRepositoryInterface
{

    public function createDelayReport(array $data)
    {
        return DelayReport::create($data);
    }

    public function getListOfVendorsDelay()
    {
        $weekAgo = Carbon::now()->subWeek();

        return DB::table('delay_reports')
            ->join('orders', 'delay_reports.order_id', '=', 'orders.id')
            ->join('vendors', 'orders.vendor_id', '=', 'vendors.id')
            ->select(DB::raw('SUM(delay_reports.delay_time) as time'), 'orders.vendor_id', 'vendors.name')
            ->where('orders.created_at', '>=', $weekAgo)
            ->groupBy('orders.vendor_id')
            ->orderByDesc('time')
            ->get();
    }
}
