<?php

namespace App\Repositories\DelayReport;

interface DelayReportRepositoryInterface
{
    public function createDelayReport(array $data);
    public function getListOfVendorsDelay();
}
