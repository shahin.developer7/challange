<?php

namespace App\Enums\Traits;

trait EnumTrait
{
    public abstract static function toArray();
    public abstract static function enum();
    public static function getName($key): string
    {
        return self::enum()->toArray()[$key];
    }

    public static function getValue($name): string
    {
        return array_search($name, (self::enum())->toArray());
    }

    public static function getValues(): array
    {
        return array_keys((self::enum())->toArray());
    }

    public static function getNames(): array
    {
        return array_values((self::enum())->toArray());
    }
}
