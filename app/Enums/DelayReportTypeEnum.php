<?php

namespace App\Enums;

use App\Enums\Traits\EnumTrait;

class DelayReportTypeEnum
{
    use EnumTrait;

    /**
     * Defined your const enum items
     */
    public const MANUAL     = 1;
    public const SYSTEM     = 2;

    /**
     * Defined your key and value
     * @return string[]
     */
    public static function toArray(): array
    {
        return [
            self::MANUAL    =>  'Manual',
            self::SYSTEM    =>  'System',
        ];
    }

    public static function enum()
    {
        return new DelayReportTypeEnum();
    }
}
