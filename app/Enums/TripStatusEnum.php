<?php

namespace App\Enums;

use App\Enums\Traits\EnumTrait;

class TripStatusEnum
{
    use EnumTrait;


    public const ASSIGNED       = 1;
    public const AT_VENDOR      = 2;
    public const PICKED         = 3;
    public const DELIVERED      = 4;

    /**
     * Defined your key and value
     * @return string[]
     */
    public static function toArray(): array
    {
        return [
            self::ASSIGNED      =>  'Assigned',
            self::AT_VENDOR     =>  'At Vendor',
            self::PICKED        =>  'Picked',
            self::DELIVERED     =>  'Delivered',
        ];
    }

    public static function enum()
    {
        return new TripStatusEnum();
    }
}
