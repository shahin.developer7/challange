<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class MakeEnum extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:enum {name}';


    public function getStub()
    {
        return resource_path('stubs\DummyEnum.stub');
    }

    public function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Enums';
    }

    public function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        $args = explode('\\',$name);
        return str_replace("DummyEnum", last($args), $stub);
    }
}
