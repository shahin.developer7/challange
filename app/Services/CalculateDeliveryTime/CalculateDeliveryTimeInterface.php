<?php

namespace App\Services\CalculateDeliveryTime;

interface CalculateDeliveryTimeInterface
{
    public function getDeliveryTime();
}
