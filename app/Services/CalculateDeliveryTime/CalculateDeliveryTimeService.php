<?php

namespace App\Services\CalculateDeliveryTime;

use Illuminate\Support\Facades\Http;

class CalculateDeliveryTimeService implements CalculateDeliveryTimeInterface
{
    public $apiUrl;
    public function __construct()
    {
        $this->apiUrl = "http://run.mocky.io/v3/";
    }

    public function getDeliveryTime()
    {
        try {
            $response = Http::retry(3, 100)->withHeaders([
                'Content-Type'  =>  'application/json',
            ])->get($this->apiUrl.'122c2796-5df4-461c-ab75-87c1192b17f7');
            $result = $response->json();
            if ($result["status"])
            {
                return $result["data"]["eta"];
            }
        }
        catch (\Exception $ex) {
            dd($ex->getMessage());
        }
    }
}
