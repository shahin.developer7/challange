<?php

namespace App\Services\Agent;

use App\Models\Agent;
use App\Models\DelayQueue;
use App\Repositories\Agent\AgentRepositoryInterface;
use App\Repositories\DelayQueue\DelayQueueRepositoryInterface;

class AgentService implements AgentServiceInterface
{
    public $agentRepository;
    public $delayQueueRepository;

    public function __construct(AgentRepositoryInterface $agentRepository, DelayQueueRepositoryInterface $delayQueueRepository)
    {
        $this->agentRepository = $agentRepository;
        $this->delayQueueRepository = $delayQueueRepository;
    }

    public function assignToOrder($agent_id)
    {
        if ($this->agentRepository->allDelayQueuePendingCount($agent_id) > 0)
            return [
                "result"    =>  false,
                "message"   =>  "not assign",
                "data"      =>  null
            ];

        $selectedDelay = $this->delayQueueRepository->getDelayOrderFromQueue();
        if ($selectedDelay == null)
            return [
                "result"    =>  false,
                "message"   =>  "not assign",
                "data"      =>  null
            ];

            $this->delayQueueRepository->updateDelayQueue($selectedDelay,[
                "agent_id"  =>  $agent_id
            ]);

        return [
            "result"    =>  true,
            "message"   =>  "assign",
            "data"      =>  null
        ];
    }
}
