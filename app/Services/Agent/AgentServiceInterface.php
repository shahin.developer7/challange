<?php

namespace App\Services\Agent;

interface AgentServiceInterface
{
    public function assignToOrder($agent_id);
}
