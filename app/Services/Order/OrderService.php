<?php

namespace App\Services\Order;

use App\Enums\DelayReportTypeEnum;
use App\Models\DelayQueue;
use App\Models\DelayReport;
use App\Models\Order;
use App\Models\Trip;
use App\Repositories\DelayQueue\DelayQueueRepositoryInterface;
use App\Repositories\DelayReport\DelayReportRepositoryInterface;
use App\Repositories\Order\OrderRepositoryInterface;
use App\Services\CalculateDeliveryTime\CalculateDeliveryTimeInterface;
use Carbon\Carbon;

class OrderService implements OrderServiceInterface
{
    public  $orderRepository;
    public  $calculateDeliveryTime;
    public  $delayQueueRepository;
    public  $delayReportRepository;

    public function __construct(OrderRepositoryInterface $orderRepository,
                                CalculateDeliveryTimeInterface $calculateDeliveryTime,
                                DelayQueueRepositoryInterface $delayQueueRepository,
                                DelayReportRepositoryInterface $delayReportRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->calculateDeliveryTime = $calculateDeliveryTime;
        $this->delayQueueRepository = $delayQueueRepository;
        $this->delayReportRepository = $delayReportRepository;
    }

    public function orderDelaySubmit($orderId)
    {
        $order = $this->orderRepository->getOrderWithTripById($orderId);

        if (Carbon::make($order->created_at)->addMinutes($order->time_delivery) < Carbon::now())
        {
            if ($this->delayQueueRepository->getCountOfOrderPendingDelay($order->id) > 0)
            return [
                    "result"    =>  false,
                    "message"   =>  "This order is in pending status",
                    "data"      =>  null
                ];

            if ($order->trip != null && in_array($order->trip->status, Trip::NewDelayStatues))
            {
                $delayTime = $this->calculateDeliveryTime->getDeliveryTime();

                $this->delayReportRepository->createDelayReport([
                    "order_id"      =>  $order->id,
                    "delay_time"    =>  $delayTime,
                    "type"          =>  DelayReportTypeEnum::SYSTEM
                ]);

                $order = $this->orderRepository->orderUpdate($order,[
                    "time_delivery" =>  $delayTime
                ]);

                return [
                    "result"    =>  true,
                    "message"   =>  "System Report",
                    "data"      =>  null
                ];
            }
            elseif($order->trip == null || in_array($order->trip->status ?? "", Trip::DelayQueueStatues))
            {
                $delayReport = $this->delayReportRepository->createDelayReport([
                    "order_id"      =>  $order->id,
                    "delay_time"    =>  0,
                    "type"          =>  DelayReportTypeEnum::MANUAL
                ]);

                $this->delayQueueRepository->addDelayQueue([
                    "delay_report_id"   => $delayReport->id,
                    "order_id"          => $order->id,
                ]);

                return [
                    "result"    =>  true,
                    "message"   =>  "Manual Report",
                    "data"      =>  null
                ];
            }
            else
            {
                return [
                    "result"    =>  true,
                    "message"   =>  "Check ...",
                    "data"      =>  null
                ];
            }
        }
        else
        {
            return [
                "result"    =>  true,
                "message"   =>  "no submit",
                "data"      =>  null
            ];
        }
    }

    public function vendorOrderDelayReport()
    {
        return $this->delayReportRepository->getListOfVendorsDelay();
    }
}
