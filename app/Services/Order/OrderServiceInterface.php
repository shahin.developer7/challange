<?php

namespace App\Services\Order;

interface OrderServiceInterface
{
    public function orderDelaySubmit($orderOrderId);
    public function vendorOrderDelayReport();
}
