<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Trip::class, function (Faker $faker) {
    return [
        "status"    =>  array_rand(\App\Enums\TripStatusEnum::getValues())
    ];
});
