<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDelayQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delay_queue', function (Blueprint $table) {
            $table->id();
            $table->foreignId("order_id")->references('id')->on('orders')->onDelete('cascade');
            $table->foreignId("delay_report_id")->references('id')->on('delay_reports')->onDelete('cascade');
            $table->foreignId("agent_id")->nullable()->references('id')->on('agents')->onDelete('cascade');
            $table->boolean("done")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delay_queue');
    }
}
