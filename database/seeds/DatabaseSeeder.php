<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Agent::class, 5)->create();
        $vendor = factory(\App\Models\Vendor::class, 10)
            ->create()
            ->each(function ($vendor) {
                $vendor->orders()->save(factory(\App\Models\Order::class)->make());
            });
        foreach (\App\Models\Order::all() as $order)
        {
            if ($order->trip == null && array_rand([0,1]) == 1)
            {
                \App\Models\Trip::create([
                    "order_id"  => $order->id,
                    "status"    => rand(1,count(\App\Enums\TripStatusEnum::toArray()))
                ]);
            }
        }
    }
}
