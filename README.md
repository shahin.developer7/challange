## About Project Structure

- Route => ./routes/api-v1.php
- Service => ./app/Services
- Repository Layer => ./app/Repositories
- Enums => ./app/Enums
- Postman file => ./doc/api.postman_collection.json


## Installation #1
- use laragon
- git clone 
- set in laragon directory
- composer update
- mv .env.example .env
- set database in .env
- php artisan migrate
- php artisan db:seed
- run
- 
## Installation #2
- use docker (for docker desktop use wsl2)
- git clone
- mv .env.example .env
- set database in .env
- docker-compose up -d
- docker-compose exec api composer update
- docker-compose exec api  php artisan migrate
- docker-compose exec api  php artisan db:seed
- run

## Test
- php artisan test
